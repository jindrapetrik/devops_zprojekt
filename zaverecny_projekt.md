# Gitlab CI/CD
**Závěrečný projekt z kurzu Devops konaného 6.12.2023-7.2.2024.**

**Autor**: Jindřich Petřík

## Úvod
Gitlab je úložiště gitu dostupné na adrese https://gitlab.com
s dokumentací dostupnou na https://docs.gitlab.com/

Gitlab je k dispozici ve verzi zdarma pro osobní použití, nebo Premium/Ultimate pro komerční použití. Existuje také community edition, kterou si lze nainstalovat na vlastní server a provozovat ji též zdarma.

Jako většina online úložišť gitu, i Gitlab podporuje formu continuous integration a continuous deyployment. Určitým konkrétním způsobem lze definovat sadu operací, která se provede například po pushi commitu do větve, nebo před mergem do hlavní větve. Taková sada operací se nazývá pipelina (pipeline).

## Konfigurační soubor
Na GitLabu je možné definovat CI/CD pipeliny pomocí speciálního souboru ve formátu YAML umístěného v kořeni git repozitáře, který se nazývá `.gitlab-ci.yml`.

V takovém souboru se definuje:
 * úkoly (task), které se mají provést
 * další includované soubory
 * závislosti a cache
 * v jakém pořadí provádět akce
 * při jaké akci dané úkoly spustit
 
Krom ruční editace v repozitáři lze využít online editor v menu `Build > Pipeline editor`,
který nabízí kontextovou nápovědu ke klíčovým slovům.

## Runnery a Executory
Runner je instance, která provádí běhy pipeline v GitLabu. Může to být fyzický server, virtuální stroj nebo kontejner. Je konfigurován tak, aby naslouchal na určitém URL a čekal na instrukce pro spuštění pipeline. Runner může být sdílený mezi více projektů nebo může být specifický pro jeden projekt. Může být nakonfigurován tak, aby podporoval různé platformy, jako jsou Linux, Windows nebo macOS.
Součást Runneru je executor, který definuje prostředí, ve kterém se spouští pipeline joby. Určuje, jakým způsobem jsou joby spuštěny a vykonávány, například pomocí Docker kontejneru, shell skriptu atd. GitLab podporuje různé typy executorů, včetně „shell“, „docker“, „parallels“, „ssh“ atd.
Každý job v pipeline může mít vlastního executora.

## Rozvržení pipeliny
V konfiguračním souboru se definují pro pipelinu tzv. joby, které určují, co se bude provádět. Joby se seskupují do tzv. stagí (stage). Stage je typicky například „build“, „test“ či „deploy“.

Ukázkový konfigurační soubor:
```yaml
build-job:
  stage: build
  script:
    - echo "Ahoj uživateli, $GITLAB_USER_LOGIN!"

test-job1:
  stage: test
  script:
    - echo "Tenhle job něco testuje"

test-job2:
  stage: test
  script:
    - echo "Tenhle job taky testuje a dlouho trvá"
    - sleep 20

deploy-prod:
  stage: deploy
  script:
    - echo "Tenhle job něco nasazuje z větve $CI_COMMIT_BRANCH"
  environment: production
```
V tomto souboru jsou klíče "build-job", "test-job1", "test-job2", "deploy-prod" identifikátory jednotlivých jobů. Podlíč `stage` určuje skupinu jobů. Sekce `skript` obsahuje sadu příkazů, které se spouští.

Ukázka běhu 3 stagí:
![Tři stage](img01_tri_stage.png)

Postup si lze zobrazit graficky:
![Vizuální reprezentace](img02_vizualni_reprezentace.png)

Vlastní běh jednotlivého jobu:
![Vlastní běh](img03_detaily_jobu.png)

## Vlastnosti
Stage popisují sekvenční zpracování. Jednotlivé joby uvnitř stage beží paralelně, pokud jsou k dispozici Runnery.

## Důležitá klíčová slova
### Rules
Slovo `rules` lze použít pro určení kdy spustit či přeskočit joby.

Příklad:

```yaml
job:
  script: echo "Ahoj, Rules!"
  rules:
    - if: $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ /^feature/ && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME != $CI_DEFAULT_BRANCH
      when: never
    - if: $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ /^feature/
      when: manual
      allow_failure: true
    - if: $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME
```

### Cache
Pomocí `cache` můžeme definovat sadu souborů a složek, které se cachují mezi během jednotlivých jobů. Cache jsou sdílené mezi pipelinami a joby.
```yaml
my_job:
  script:
    - echo "Tenhle job používá cache."
  cache:
    key: vendor-cache
    paths:
      - vendor/
      - myfile.phar
```
Klíč (key) je identifikátor, pod kterým bude cache uložena/načtena. Tento identifikátor může být definován i jako soubor/y.
```yaml
my-job:
  script:
    - echo "Tenhle job používá cache."
  cache:
    key:
      files:
        - composer.lock
    paths:
      - vendor/
```

### Artifacts
Klíčové slovo `artifacts` se používá pro uložení jobových artefaktů. Artefakt je seznam vybraných souborů či složek, které jsou přiloženy k jobu, pokud úspěšně doběhne (případně neúšpěšně, nebo obojí).
Artefakty jsou poslány do GitLabu, po dokončení jobu.
```yaml
job:
  artifacts:
    paths:
      - binarky/
      - .config
    exclude:
      - binaries/**/*.o
    expire_in: 1 week
    when: on_success
```

### Default
Slovem `default` lze nastavit konfiguraci, která je platná pro všechny joby. 
```yaml
default:
  image: ruby:3.0
  retry: 2

rspec:
  script: bundle exec rspec

rspec 2.7:
  image: ruby:2.7
  script: bundle exec rspec
```

### Before/after script
`before_script` a `after_script` nastavuje co je spouštěno před/po hlavní akcí jobu `script`. Typicky se používá v kombinaci s `default`.

### Variables
Proměnné pro skript se nastavují slovem `variables`.
```yaml
variables:
  DEPLOY_SITE: "https://example.com/"
  DEPLOY_ENVIRONMENT:
    value: "staging"
    description: "The deployment target. Change this variable to 'canary' or 'production' if needed."

deploy_job:
  stage: deploy
  script:
    - deploy-script --url $DEPLOY_SITE --path "/"
  environment: production
```

### Stages
Direktiva `stages` určuje pořadí vykonávání jednotlivých stages.
```yaml
stages:
  - build
  - test
  - deploy
```

### Image
`image` určuje v jakém Docker obrazu poběží daný job
```
image:
  name: "registry.example.com/my/image:latest"
```

### Include
Slovem `include` lze vložit jiný konfigurační soubor.
```yaml
include: '.gitlab-ci-production.yml'
```
Lze použít i include z jiného projektu na stejném Gitlabu:
```yaml
include:
    - project: 'my-group/my-project'
      ref: main #volitelně branch, tag nebo sha
      file: '.gitlab-ci-production.yml'
```
Nebo ze vzdáleného projektu:
```yaml
include:
  - remote: 'https://gitlab.com/example-project/-/raw/main/.gitlab-ci.yml'
```

### Spec - inputs
Pokud chceme vkládat `includem` jiné soubory, je k dispozici také konstrukce pomocí `spec` a `inputs`, která definuje vstupní argumenty pro případ, kdy je soubor includován. Tím dosáhneme efektivního znovupoužitelného kódu.

```yaml
spec:
  inputs:
    job-prefix:     # Povinný string vstup
      description: "Define a prefix for the job name"
    job-stage:      # Volitelný string vstup s výchozí hodnotou při nezadání
      default: test
    environment:    # Povinný vstup který musí být jeden z optionů
      options: ['test', 'staging', 'production']
    concurrency:
      type: number  # Volitelný numerický vstup s výchozí hodnotou při nezadání
      default: 1
    version:        # Povinný string vstup který musí projít regulárním výrazem
      type: string
      regex: /^v\d\.\d+(\.\d+)$/
    export_results: # Volitelný boolean vstup s výchozí hodnotou při nezadání
      type: boolean
      default: true
---

"$[[ inputs.job-prefix ]]-scan-website":
  stage: $[[ inputs.job-stage ]]
  script:
    - echo "scanning website -e $[[ inputs.environment ]] -c $[[ inputs.concurrency ]] -v $[[ inputs.version ]]"
    - if [ $[[ inputs.export_results ]] ]; then echo "export results"; fi
```
Hlavička (header) musí být oddělena od zbytku pomocí `---`.
Jednotlivé hodnoty vstupů lze pak v těle používat pomocí `$[[ inputs.input-id ]]`.

Při vkládání předáme hodnoty vstupů:
```yaml
include:
  - local: 'scan-website-job.yml'
    inputs:
      job-prefix: 'some-service-'
      environment: 'staging'
      concurrency: 2
      version: 'v1.3.2'
      export_results: false
```

## Gitlab proměnné
Gitlab obsahuje některé předdefinované proměnné, které můžeme používat ve skriptech. Zde je pár těch nejzajímavějších:  

| Variable name    | Description   |
|------------------|---------------|
|`CI_COMMIT_AUTHOR` | Autor commitu |
|`CI_COMMIT_BRANCH` | Větev commitu |
|`CI_COMMIT_MESSAGE`| Zpráva commitu |
|`CI_COMMIT_TAG`| Název tagu commitu |
|`CI_JOB_ID`| Unikátní id jobu napříč všemi joby|
|`CI_JOB_STAGE`| Stage jobu|
|`CI_PROJECT_DIR`| Složka projektu |

Můžeme tak například provádět určitě joby jen pro něktré uživatele, větve, tagy, nebo další různé podmínky na základě proměnných.


V yaml souboru lze také definovat vlastní proměnné:
```yaml
variables:
  GLOBAL_VAR: "A globální proměnná"
```

Proměnné, které by měly zůstat tajné jako různé klíče, secrety, apod., by se měly editovat v nastavení projektu.
Tam lze také určit, že daná proměnná bude maskovaná ve výpisech jobu tak, aby ji nebylo možné zachytit.
![Proměnné](img04_promenne.png)

### Závěr
Gitlab CI/CD konfigurace obsahuje spoustu zajímavých nastavení,  
které je jistě dobré integrovat do pipelin.
Přehled, co zde byl uveden zdaleka není vyčerpávající. Pro další informace je vhodné nastudovat přesně aktuální dokumentaci. Některá nastavení se též s novými verzemi mohou měnit.